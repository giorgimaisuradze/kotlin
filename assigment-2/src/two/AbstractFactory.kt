interface AbstractFactory {

	fun create(type: String): Any?

}

class FactoryCreator {

	companion object {
		fun createFactory(type: String): AbstractFactory {
			return when (type) {
				"Car" -> CarFactory()
				"Aircraft" -> AircraftFactory()
				else -> throw RuntimeException("No factory found")
			}
		}
	}

}