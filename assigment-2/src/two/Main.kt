fun main() {

	val aircraftFactory: AbstractFactory = FactoryCreator.createFactory("Aircraft")

	val aircraft737: Aircraft = aircraftFactory.create("Boeing737") as Aircraft
	val aircraft777: Aircraft = aircraftFactory.create("Boeing777") as Aircraft

	println(aircraft777)
	println(aircraft737.fly())

	val carFactory: AbstractFactory = FactoryCreator.createFactory("Car")

	val audi: Car = carFactory.create("Audi") as Car
	val maserati: Car = carFactory.create("Maserati") as Car

	println(audi)
	println(maserati.drive())

}