package one

import kotlin.math.pow

fun main() {
	val point = Point(2, 2)
	val point2 = Point(1, -5)

	println(point)
	println(point == point2)
	println(point.calculateDistance(point2))
}

class Point(var x: Int, var y: Int) {

	/**
	 * Calculates distance between two points with formula: √[(x₂ - x₁)² + (y₂ - y₁)²]
	 *
	 * @return the distance
	 */
	fun calculateDistance(other: Point): Double {
		val dX: Double = (other.x - x).toDouble().pow(2)
		val dY: Double = (other.y - y).toDouble().pow(2)

		return (dX + dY).pow(0.5)
	}

	fun toSymmetricPoints() {
		this.x = -x
		this.y = -y
	}

	override fun toString(): String {
		return "Point(${x},${y})"
	}

	override fun equals(other: Any?): Boolean {
		if (other is Point) {
			if (other.x == x && other.y == y) {
				return true
			}
		}

		return false
	}

}