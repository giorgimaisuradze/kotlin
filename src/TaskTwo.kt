fun isPalindrome(str: String): Boolean {
    var withoutWhiteSpace = "";

    for (i in 0 until str.length) {
        if (str[i] != ' ') {
            withoutWhiteSpace += str[i].lowercase()
        }
    }

    val size = withoutWhiteSpace.length - 1

    for (i in 0 until withoutWhiteSpace.length / 2) {
        if (withoutWhiteSpace[i] != withoutWhiteSpace[size - i]) {
            return false
        }
    }

    return true
}

fun main() {
    val obj1 = "noon"
    val obj2 = "madam"
    val obj3 = "როგორ"
    val obj4 = "was it a cat I saw"
    val obj5 = "no palindrome"

    println(isPalindrome(obj1))
    println(isPalindrome(obj2))
    println(isPalindrome(obj3))
    println(isPalindrome(obj4))
    println(isPalindrome(obj5))
}