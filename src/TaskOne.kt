fun evenAvg(nums: IntArray): Int {
    var sum = 0
    var count = 0

    for (i in 0 until nums.size step 2) {
        sum += nums[i]
        count++
    }

    if (count != 0)
        return sum / count;

    return 0
}

fun main() {
    val result = evenAvg(intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20))

    println("Result: $result")
}
